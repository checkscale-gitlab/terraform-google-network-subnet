######### >> Gitlab-CI for Terraform Modules ------------------------------------------
# This file aims to configure Gitlab-Ci to work with terraform with GCP

## Environment Variables:
# GITLAB_TOKEN - Access Token for Gitlab

---
######### >> Global Settings ----------------------------------------------------------

image:
  name: hashicorp/terraform:0.13.0-beta3
  entrypoint:
    - "/usr/bin/env"
    - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

stages:
  - tf_format
  - init_validate
  - tag_release
  - deploy

######### >> Format (Terraform Lint) --------------------------------------------------
# This job will review the format of terraform
# Docs: https://www.terraform.io/docs/commands/fmt.html

tf_format:
  stage: tf_format
  except:
    - tags
  script: |
    #!/bin/sh
    fmt_diff=$(find . -name "*.tf" | xargs -I{} terraform fmt -write=false {} | sed '/^\s*$/d')
    if test -n "$fmt_diff"; then
      echo "******* Terraform formatting error:"
      echo ""
      echo $fmt_diff
      exit 1
    fi

######### >> Terraform Init & Validate ------------------------------------------------
# This job initializes the backend and validates the code syntax.
# Docs: https://www.terraform.io/docs/commands/validate.html

init_validate:
  stage: init_validate
  dependencies:
    - tf_format
  script:
    - terraform --version
    - terraform init -backend=false
    - terraform validate
  except:
    - tags


######### >> Tag & Release ------------------------------------------------------------
# The semantic-release uses the commit messages to determine the type of changes in the codebase.
# Following formalized conventions for commit messages, semantic-release automatically determines
# the next semantic version number, generates a changelog and publishes the release.
# By default semantic-release uses Angular Commit Message Conventions.
# Source: https://github.com/semantic-release/semantic-release
# Docs: https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines

tag_release:
  stage: tag_release
  image: brunosb/semantic-delivery-gitlab:latest
  before_script:
    - git fetch --tags -f
  script:
    - semantic-delivery-gitlab --token $GITLAB_TOKEN
  when: manual
  only:
    - master

######### >> Terraform Module Deploy ------------------------------------------
# This job deploy the module packet to Gitlab Terraform Module Registry
# Docs: https://docs.gitlab.com/ee/user/packages/terraform_module_registry/

deploy:
  stage: deploy
  image: curlimages/curl:latest
  dependencies:
    - tag_release
  variables:
    TERRAFORM_MODULE_DIR: ${CI_PROJECT_DIR} # The path to your Terraform module
    TERRAFORM_MODULE_NAME: ${CI_PROJECT_NAME} # The name of your Terraform module
    TERRAFORM_MODULE_SYSTEM: gcp # The system or provider your Terraform module targets (ex. local, aws, google)
    TERRAFORM_MODULE_VERSION: ${CI_COMMIT_TAG} # The version of your Terraform module to be published to your project's registry
  script:
    - tar -cvzf ${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz -C ${TERRAFORM_MODULE_DIR} --exclude=./.git .
    - 'curl -vvvvvvv --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${TERRAFORM_MODULE_NAME}/${TERRAFORM_MODULE_SYSTEM}/${TERRAFORM_MODULE_VERSION}/file'
